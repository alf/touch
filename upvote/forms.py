from django import forms

class VoteForm(forms.Form):
    description = forms.CharField(widget=forms.Textarea(), max_length=250, label="Suggestion")
    taskid = forms.CharField(max_length=10, label="Phabricator Task ID:")
    