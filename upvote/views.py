from django.shortcuts import render
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.db.models import Count

from people.models import User
from .forms import VoteForm
from .models import Vote

@login_required
def index(request):

    if request.method=='POST':
        creator = request.user
        form = VoteForm(request.POST)
        if form.is_valid():
            vote = Vote(creator = creator, description=form.cleaned_data["description"], taskid=form.cleaned_data["taskid"], timestamp=timezone.now())
            vote.save()
            vote.voters.add(creator)
            vote.save()

            form = VoteForm()
    else:
        form = VoteForm()


    # approve response #XXX this could be done better - in REST?
    if request.GET and 'id' in request.GET.keys():
        vote = Vote.objects.filter(pk=request.GET['id'])[0]
        vote.voters.add(request.user)
        vote.save()
    
    votes = Vote.objects.annotate(num_votes=Count('voters')).order_by('-num_votes')

    context = { 'form' : form, 'votes' : votes } 
    return render(request, 'upvote/index.html', context)
