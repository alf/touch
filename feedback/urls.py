from django.urls import path

from . import views

app_name = 'feedback'
urlpatterns = [
	path('', views.index, name='index'),
	path('done/', views.submit, name='done'),
	path('review/', views.ReviewView.as_view(template_name='feedback/review_list.html'), name='review'),
	path('my/', views.MyView.as_view(template_name='feedback/my_list.html'), name='my'),
	path('edit/<int:id>', views.edit, name='edit'),
]
