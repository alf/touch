from django.db import models
from people.models import User

class Response(models.Model):
    giver = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="feedback_giver")
    receiver = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="feedback_receiver")
    text = models.CharField(max_length=1000)
    is_anon = models.BooleanField(default=False)
    timestamp = models.DateTimeField('timestamp')
    ready = models.BooleanField(default=False)
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.giver.username + "-" + self.receiver.username
