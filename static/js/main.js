$(document).ready(function() {
    $('#add-feedback').select2({
        placeholder: 'Add someone +',
    });

    $('#add-feedback').change(function(e) {
        $(
        "<label class='subject-label hide' for='subject'>"+ e.target.value +"</label>" +
	"(<span class='checkbox'><input type='checkbox' name='" + e.target.value  + "_anon' value='Bike'>Anonymous</span>)" +
        "<textarea id='"+ e.target.value +"' name='"+ e.target.value +"' placeholder='Write something..'></textarea>"
        ).hide().appendTo('#new-feedbacks').fadeIn("slow");;

        // Remove selected person from the list
        $(this).children('#opt-' + e.target.value).remove();
        // Set select value to blank
        $(this).val('');
    });

    $('.hot-feedback').click(function() {
        var person = $(this).attr('value');
        $(
            "<label class='subject-label hide' for='subject'>"+  person +"</label>" +
        "(<span class='checkbox'><input type='checkbox' name='" + person  + "_anon' value='Bike'>Anonymous</span>)" +
            "<textarea id='"+ person +"' name='"+ person +"' placeholder='Write something..'></textarea>"
            ).hide().appendTo('#new-feedbacks').fadeIn("slow");

        $(this).hide()
    });
});
