from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'people'
urlpatterns = [
	path('', views.index, name='people'),
	path('login/', auth_views.LoginView.as_view(template_name='people/login.html')),
	path('logout/', auth_views.LogoutView.as_view(template_name='people/login.html')),
	path('apikey/', views.apikey, name='apikey'),
	path('mentees/', views.MenteesView.as_view(template_name='people/mentees_list.html'), name='mentees'),
	path('<str:username>/', views.profile, name='profile'),

]
