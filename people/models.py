from django.db import models
from django.contrib.auth.models import AbstractUser

class Department(models.Model):
    name = models.CharField(max_length=100)
    lead = models.ForeignKey('User', on_delete=models.DO_NOTHING, related_name="lead")
    def __str__(self):
        return self.name

class User(AbstractUser):
    """
    A user of touch who will be using this system.
    """
    #XXX it was breaking oauth, we will redo user class anyway
    #department = models.ForeignKey(Department, default=0, on_delete=models.DO_NOTHING)
    is_active = models.BooleanField(default=True)
    mentor = models.ForeignKey('User', on_delete=models.DO_NOTHING, blank=True, null=True)

    def name(self):
        return self.first_name + ' ' + self.last_name

class Team(models.Model):
    name = models.CharField(max_length=100)
    people = models.ManyToManyField(User)
    reviewer = models.ManyToManyField(User, related_name="reviewer", blank=True, null=True)
    def __str__(self):
        return self.name

