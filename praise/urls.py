from django.urls import path

from . import views

app_name = 'praise'
urlpatterns = [
	path('', views.index, name='index'),
]
