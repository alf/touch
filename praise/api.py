from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication
from tastypie import fields

from .models import Praise
from people.models import User
from people.api import UserResource

class PraiseResource(ModelResource):
    giver_id = fields.IntegerField(attribute='giver_id')
    receiver = fields.ForeignKey(UserResource, attribute='receiver', full=True) 
    client = fields.CharField(attribute='client')

    class Meta:
        queryset = Praise.objects.all()
        resource_name = 'praise'
        fields = ['giver', 'receiver', 'text']
        limit = 200
        authorization = Authorization()
        authentication = ApiKeyAuthentication()

    def hydrate(self, bundle):
        user = User.objects.filter(username=bundle.request.user)[0]
        bundle.data['giver_id'] = user.id
        bundle.data['client'] = 'api'
        return bundle
