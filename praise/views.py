from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Count

import requests
import json
from datetime import datetime, timedelta

from .models import Praise
from people.models import User
from .forms import PraiseForm

def post_kudos_url(giver, receiver, text):
    headers = {'Content-type': 'application/json'}
    post_data = {}
    post_data['icon_url'] = "https://touch.collabora.com/static/img/favicon.ico"
    post_data['text'] = ":tada: :tada: :tada: [**kudos!**](https://touch.collabora.com/praise/) - from @" + giver.username + " to :medal_sports: **@" + receiver.username + "** \n>" + text
    r = requests.post(settings.KUDOS_WEBHOOK, json.dumps(post_data), headers=headers)

def send_kudos_email(praise):
    message = '''
    Hello, {0}

    You have received a new kudos from {1}:

    "{2}"

    Go to https://touch.collabora.com/praise/ to see all kudos.

    Thank you for being part of our Feedback Culture! Give feedback to someone at https://touch.collabora.com/feedback.
    '''
    send_mail(
        'You received a new kudos!',
         message.format(praise.receiver.first_name, praise.giver.name(), praise.text),
        'noreply@touch.collabora.com',
        [praise.receiver.username + '@collabora.com'],
        fail_silently=False,
    )

def choices_list(giver):
    l = []
    for u in User.objects.exclude(username=giver.username).filter(is_active=True).order_by('first_name'):
        l.append(tuple((u.id, u.name)))
    return l

@login_required
def index(request):
    giver = request.user

    if request.method=='POST':
        form = PraiseForm(request.POST)
        form.fields['receiver'].choices = choices_list(giver)
        if form.is_valid():
             receiver = User.objects.filter(id=form.cleaned_data['receiver'])[0]
             praise = Praise(giver=giver, receiver=receiver, text=form.cleaned_data["text"], timestamp=timezone.now(), client="web")
             praise.save()

             post_kudos_url(giver, receiver, form.cleaned_data["text"])
             form = PraiseForm()
             send_kudos_email(praise)
    else:
        form = PraiseForm()

    kudos = Praise.objects.order_by('timestamp').reverse()

    form.fields['receiver'].choices = choices_list(giver)
    context =  {'kudos': kudos, 'form': form }

    all_top_receivers = User.objects.annotate(num_kudos=Count('praise_receiver')).order_by('-num_kudos')[:10]
    all_top_givers = User.objects.annotate(num_kudos=Count('praise_giver')).order_by('-num_kudos')[:10]

    period = datetime.today() - timedelta(days=90)
    last3_top_receivers = User.objects.filter(praise_receiver__timestamp__gte=period).annotate(num_kudos=Count('praise_receiver')).order_by('-num_kudos')[:10]
    last3_top_givers = User.objects.filter(praise_giver__timestamp__gte=period).annotate(num_kudos=Count('praise_giver')).order_by('-num_kudos')[:10]

    context['all_top_receivers'] = all_top_receivers
    context['all_top_givers'] = all_top_givers
    context['last3_top_receivers'] = last3_top_receivers
    context['last3_top_givers'] = last3_top_givers

    return render(request, 'praise/index.html', context)
